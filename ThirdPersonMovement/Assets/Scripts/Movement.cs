﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float PlayerSpeed=5.0f;
	public float JumpSpeed=20.0f;
	private float gravity=9.8f;
	private CharacterController _controller;
	private Transform _myTransform;
	private float rotSpeed;
	Vector3 moveDir=Vector3.zero;

	void Awake(){
		_controller = GetComponent<CharacterController> ();
		_myTransform = transform;
	}

	void Start () {
		rotSpeed=GameObject.Find ("ThirdPersonCamera").GetComponent<ThirdPersonCamera> ().ms;

	}

	// Update is called once per frame
	void Update () {

		moveDir = new Vector3 (Input.GetAxis ("Horizontal"), 0.0f, Input.GetAxis ("Vertical"));

		transform.Rotate (0.0f, Input.GetAxis ("Mouse X")*rotSpeed, 0.0f);

		moveDir = _myTransform.TransformDirection (moveDir);


	
		_controller.Move (moveDir * Time.deltaTime * PlayerSpeed);

	}
}
